# Deploying configurable frontend web application containers

Slides for my [Docker Randstad](https://www.meetup.com/Docker-Randstad/) talk

* Demo code: <https://github.com/zemanel/doom-client>
* Blog post: <https://container-solutions.com/deploying-configurable-frontend-web-application-containers/>
