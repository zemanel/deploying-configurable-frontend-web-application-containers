---
title: Deploying configurable frontend web application containers
hideFirstSlide: true
authors: José Moreira
email: "jose.moreira@container-solutions.com"
---

# Bio

* Software Engineer @ Container Solutions
* Full-stack developer but mostly backend
* Passionate about architecture and infrastructure
* ❤ containers

---

**Deploying configurable frontend web application containers**

---

## Agenda

* Deploying backend containers
* Web application configuration
* Meta tags approach
* Application level code
* Deployment scripts
* Kubernetes deployment
* Local development environment
* Demo
* Questions?

---

## My experiences deploying web applications

---

## Deploying backend containers

```yaml
# docker-compose.yaml
version: '3'
services:
  backend:
    image: example/api:latest
    ports:
    - "8080:8080"
    environment:
      - DB_USER=user
      - DB_PASS=secret_password
      - DB_NAME=exampledb
    depends_on:
    - db
  db:
    image: sameersbn/postgresql:10
    ports:
      - "5432:5432"
    environment:
      - DB_USER=user
      - DB_PASS=secret_password
      - DB_NAME=exampledb
```

---

## Docker images

Build once, run anywhere

---

## Treating frontend containers like backend containers

---

## How client-side applications work

* Runtime is the client Web browser
* Client-side applications cannot read environment variables or configuration files
* Source code is bundled and served from CDN or application web servers
* Configuration values are "hardcoded" at build time

---

Webpack asset build

![Building assets](images/assetbuild.gif)

---

## Web Application configuration points

Configuration values change per environment (development, staging, production):

* API URLs
* API tokens
* Analytics tracking codes (client IDs)
* Application parameters

---

## Deploying web application with containers

Some approaches to configuration per environment

---

## Dockerfile

```dockerfile
FROM node:8.11.4-jessie
RUN mkdir /app
WORKDIR /app
COPY package.json .
RUN npm install
COPY . .
ENV NODE_ENV production
RUN npm run build
CMD npm run dev
```

---

## Rebuild assets before deployment (inside Docker container)

* Adds to deployment time. Depending on deployment rate and size of the project, the deployment time overhead might be considerable.
* Is prone to build failures at end of deployment pipeline (network conditions)
* Might affect rollback speed

---

## Build one image per environment

* Adds clutter to the docker registry/daemon.
* Changing configuration implies rebuilding Docker images

---

## Loading config from HTTP endpoint

Web app requests configuration from a dynamically generated `https://myapp.com/js/config.js` file.

* Extra request per page load implies higher traffic load
* Web app cannot start business logic before loading configuration

---

## Meta tags approach

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">

    <meta property="DOOM_STATE_SERVICE_URL" content="http://localhost:8081/" />
    <meta property="DOOM_ENGINE_SERVICE_URL" content="http://localhost:8082/" />

    <link rel="icon" href="./favicon.ico">
    <title>frontend</title>
  </head>
  <body>
    <noscript>
      <strong>We're sorry but frontend doesn't work properly without JavaScript enabled. Please enable it to continue.</strong>
    </noscript>
    <div id="app"></div>
    <!-- built files will be auto injected -->
  </body>
</html>
```

---

* Configuration meta tags are added to `index.html`
* During deployment, meta tag values are rewritten based on environment variables (or mounted configuration files)
* Javascript application reads configuration from meta tags on page load

---

## Abstracting the configuration layer

---

```$ cat src/config/loader.js```

```javascript

/**
 *
 * Get config value  with precedence:
 * - check `process.env`
 * - check current web page meta tags
 * @param {string} key Configuration key name
 */
function getConfigValue (key) {
  let value = null
  if (process.env && process.env[`${key}`] !== undefined) {
    // get env var value
    value = process.env[`${key}`]
  } else {
    // get value from meta tag
    return getMetaValue(key)
  }
  return value
}

// ommited rest of code

```

---

```$ cat src/config/index.js```

```javascript

import loader from './loader'

export default {
  DOOM_STATE_SERVICE_URL: loader.getConfigValue('DOOM_STATE_SERVICE_URL'),
  DOOM_ENGINE_SERVICE_URL: loader.getConfigValue('DOOM_ENGINE_SERVICE_URL')
}

```

anywhere on the source code

```javascript

import config from './config'
console.log(config.DOOM_ENGINE_SERVICE_URL)

```

---

## Kubernetes deployment

![architecture](images/configurable-frontend-application-architecture.png)

* Deployment helper script:
  * Deletes existing files from public directory
  * Copies `dist` assets from Docker image to public directory
  * Rewrites config meta tags on public directory `index.html`

---

`$cat bin/rewrite-config.js`

```javascript
#!/usr/bin/env node

const cheerio = require('cheerio')
const copy = require('recursive-copy')
const fs = require('fs')
const rimraf = require('rimraf')

const DIST_DIR = process.env.DIST_DIR
const WWW_DIR = process.env.WWW_DIR
const DOOM_STATE_SERVICE_URL = process.env.DOOM_STATE_SERVICE_URL
const DOOM_ENGINE_SERVICE_URL = process.env.DOOM_ENGINE_SERVICE_URL

// ommited rest of code

```

---

## Kubernetes deployment manifest

---

## Kubernetes init container

```yaml
initContainers:
- name: doom-client
  image: "doom-client:latest"
  command: ["/app/bin/rewrite-config.js"]
  imagePullPolicy: IfNotPresent
  env:
    - name: DIST_DIR
      value: "/app/dist"
    - name: WWW_DIR
      value: "/tmp/www"
    - name: DOOM_ENGINE_SERVICE_URL
      value: "http://localhost:8081/"
    - name: DOOM_STATE_SERVICE_URL
      value: "http://localhost:8082/"
  volumeMounts:
  - name: www-data
    mountPath: /tmp/www
```

---

## Kubernetes Container

```yaml
containers:
- name: nginx
  image: nginx:1.14
  imagePullPolicy: IfNotPresent
  volumeMounts:
  - name: www-data
    mountPath: /usr/share/nginx/html
  - name: doom-client-nginx-vol
    mountPath: /etc/nginx/conf.d
```

---

## Kubernetes shared persistent volume

```yaml

  volumes:
    - name: www-data
      emptyDir: {}
    - name: doom-client-nginx-vol
      configMap:
        name: doom-client-nginx
```

---

# Local development environment

* Docker Compose services:
  * webpack dev server
  * backend API services
  * NGINX instance for production build testing

---

# UI service (webpack dev server)

```yaml

version: '3'
services:

 ui:
   build: .
   command: ["npm", "run", "dev", ]
   ports:
     - "8080:8080"
   environment:
   - HOST=0.0.0.0
   - PORT=8080
   - NODE_ENV=development
   - DOOM_ENGINE_SERVICE_URL=http://localhost:8081/
   - DOOM_STATE_SERVICE_URL=http://localhost:8082/
   volumes:
   - .:/app
   # bind volume inside container for source mount not shadow image dirs
   - /app/node_modules
   - /app/dist
```

---

## Backend services

```yaml

 doom-engine:
   image: microservice-doom/doom-engine:latest
   environment:
     - DOOM_STATE_SERVICE_URL=http://doom-state:8080/
     - DOOM_STATE_SERVICE_PASSWORD=enginepwd
   ports:
     - "8081:8080"

 doom-state:
   image: microservice-doom/doom-state:latest
   ports:
     - "8082:8080"
```

---

## Production build test services

```yaml

 ui-deployment:
   build: .
   command: ["/app/bin/rewrite-config.js"]
   environment:
     - NODE_ENV=production
     - DIST_DIR=/app/dist
     - WWW_DIR=/tmp/www
     - DOOM_ENGINE_SERVICE_URL=http://localhost:8081/
     - DOOM_STATE_SERVICE_URL=http://localhost:8082/
   volumes:
   - .:/app
   # bind volume inside container for source mount not shadow image dirs
   - /app/node_modules
   - /app/dist
   # shared NGINX static files dir
   - www-data:/tmp/www
   depends_on:
     - nginx
```

---

## Test production build with NGINX

```yaml

 nginx:
   image: nginx:1.14
   ports:
     - "8090:80"
   volumes:
   - www-data:/usr/share/nginx/html

# shared persistent volume
volumes:
 www-data:
```

---

# Demo

---

# Shameless plug

<https://container-solutions.com/deploying-configurable-frontend-web-application-containers/>

---

![Hiring](images/Presentation_slide.png)

---

## Keep in touch!

* Twitter: @zemanel
* E-mail: jose.moreira@container-solutions.com
* LinkedIn: https://www.linkedin.com/in/josemoreira

---

## Questions
